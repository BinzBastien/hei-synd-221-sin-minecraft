package ch.hevs.isi.web;

import ch.hevs.isi.field.BooleanRegister;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.field.FloatRegister;
import ch.hevs.isi.field.ModbusAccessor;

import java.util.Timer;
import java.util.TimerTask;



public class SmartControl {

    private static SmartControl sc = null;

    /** If no instance, create an unique instance of the class. If an instance exist, it return it to avoid multiple instances.
     *
     * @return The instance of the Smart control.
     */
    public static SmartControl getInstance(){
        if (sc == null) {
            sc = new SmartControl();
        }
        return sc;
    }

    /** initiate a new PollTimer
     *
     */
    public SmartControl() {
        Timer pollTimer = new Timer();
        pollTimer.scheduleAtFixedRate(new PollTask(), 0, 1000);
    }

    /** Read the score of the simulation
     *
     * @return
     */
    public float getScore() {
        ModbusAccessor ma = ModbusAccessor.getInstance();
        return ma.readFloat(345);
    }

    /**Return the value of the register
     *
     * @param regAddress specify the register
     * @return
     */
    public float getValue(int regAddress) {
        ModbusAccessor ma = ModbusAccessor.getInstance();
        return ma.readFloat(regAddress);
    }

    /** change the value of the register
     *
     * @param regAddress specify the register
     * @param value
     */
    public void setValue(int regAddress, float value) {
        ModbusAccessor ma = ModbusAccessor.getInstance();
        ma.writeFloat(regAddress, value);
    }

    /** change the value of the register
     *
     * @param regAddress specify the register
     * @param value
     */
    public void setValue(int regAddress, boolean value) {
        ModbusAccessor ma = ModbusAccessor.getInstance();
        ma.writeBoolean(regAddress, value);
    }

    /** Regulation energy for the Minecraft World
     *The goal is to have the same consommation and the same production
     */
    private static class PollTask extends TimerTask {
        @Override
        public void run() {

            SmartControl sc = SmartControl.getInstance();
            float battery = sc.getValue(49);

            System.out.println(battery * 100);

            float power_solar = sc.getValue(61);
            float power_wind = sc.getValue(53);
            float power_coal = sc.getValue(81);
            float weather = sc.getValue(317);       //time in game
            float power_factory = sc.getValue(105);
            float coal = sc.getValue(65);           // stock of coal
            float difference =power_factory-(power_coal + power_solar + power_wind);
            sc.setValue(401, true); // Solar ON
            sc.setValue(405, true); // Wind ON
            int day = 0;

            if(difference<0){                                                                     //consomation>production
                    if (sc.getValue(205) < 1.f) {
                        sc.setValue(205, sc.getValue(205) + 0.01f); // Factory regulation
                    }
                    if (sc.getValue(65) < 0.4f) {
                        sc.setValue(209, 0.f); // Coal 0%
                    }
                    else if (battery>0.55f) {
                        sc.setValue(209, 0.f); //coal 0%
                    }
                    else {
                        sc.setValue(209, 0.2f); //coal factory regulation
                    }
                }
                else {                                                                            //production>consomation
                    if (sc.getValue(205) >difference) {
                        sc.setValue(205, sc.getValue(205) - 0.1f); // Factory regulation
                    }

                    if (battery<0.55f) {
                        sc.setValue(209, sc.getValue(65)+0.01f); // Coal 0%
                    }
                    else {
                        sc.setValue(209, 0.0f); //coal factory regulation
                    }
                }


            if(coal<=0f){
                sc.setValue(209, 0.f); // Coal 0%

            }
            if(weather>0.95f &day==0) {                   //day one
                day++;
            }
            if(weather>0.94f &day==1) {                   //day two
                day++;
            }
            if(day==2&coal>0.2f){
                sc.setValue(209, 1.f); // Coal 100%

            }

            if (battery < 0.2f) {
                sc.setValue(209, 1.f); // Coal 100%
                sc.setValue(205, 0.f); // Factory 0%

            } else if (battery > 0.70f) {
                sc.setValue(401, false); // Solar OFF
                sc.setValue(405, false); // Wind OFF
                sc.setValue(205, 1.f); // Factory 100%
            }



        }
    }
}
