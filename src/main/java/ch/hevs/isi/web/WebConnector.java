package ch.hevs.isi.web;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.field.FloatRegister;
import ch.hevs.isi.utils.Utility;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.ip.tcp.TcpMaster;
import com.serotonin.modbus4j.sero.timer.SystemTimeSource;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;


import java.util.LinkedList;
import java.util.List;
import java.util.Timer;

import java.net.InetSocketAddress;
import java.util.HashMap;

public class WebConnector extends WebSocketServer {
    private final Boolean DEBUG_ENABLE	= true;

    //Singleton
    private static WebConnector wc = null;

    // All active connections
    List<WebSocket> connections = new LinkedList<>();

    private Timer _pollCoils;


    private WebConnector() {
        super(new InetSocketAddress("localhost", 8888));    // Init webSocketServer constructor parameters
        DEBUG_INFO("WebConnector()", "Started WebSocket server on Port " + String.valueOf(8888) + ".");

        start();
    }

    /** If no instance, create an unique instance of the class. If an instance exist, it return it to avoid multiple instances.
     *
     * @return The instance of the WebConnector
     */
    public static WebConnector getInstance(){
        if (wc == null) {
            wc = new WebConnector();
        }
        return wc;
    }

    /** Write to the register a new value of a FloatDataPoint.
     *
     *  @param fdp The FloatDataPoint with a new value.
     */
    public void onNewValueFloat(FloatDataPoint fdp) {
        for(int i = 0; i < connections.size(); i++)
        {
            connections.get(i).send((fdp.getLabel() + "=" + fdp.getValue()));
        }    }

    /** Write to the register a new value of a BooleanDataPoint.
     *
     * @param bdp The BooleanDataPoint with a new value.
     */
    public void onNewValueBoolean(BooleanDataPoint bdp) {
        for(int i = 0; i < connections.size(); i++)
        {
            connections.get(i).send((bdp.getLabel() + "=" + bdp.getValue()));
        }    }

    /** Start a new connection with a new web page
     *
     * @param webSocket instance for one active TCP connection in the server
     * @param clientHandshake
     */
    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        DEBUG_INFO("onOpen()", "");

        //@remove
        connections.add(webSocket);
        webSocket.send("Welcome on MineCraft Electrical Age WebSocket Server...");
        //FieldConnector.getInstance().notifyState();
    }

    /** Close the connection when the web page is closed
     *
     * @param webSocket instance for one active TCP connection in the server
     * @param i
     * @param s
     * @param b
     */
    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        DEBUG_INFO("onClose()", "");

        // Safely release the _connection
        connections.remove(webSocket);
    }

    /** Read the new value of the Datapoint in the Web page and update the other pages Web and the Field
     *
     * @param webSocket instance for one active TCP connection in the server
     * @param message contains the value
     */
    @Override
    public void onMessage(WebSocket webSocket, String message) {
        DEBUG_INFO("onMessage()", "");

        // Parse message
        if (message != null && message.contains("=")) {
            DEBUG_INFO("onMessage()", "msg: " + message);

            String[] params = message.split("=");
            String label = params[0];
            {
                if(DataPoint.getDataPointFromLabel(label) instanceof BooleanDataPoint) {
                    BooleanDataPoint dp = (BooleanDataPoint) DataPoint.getDataPointFromLabel(label);

                    boolean value = Boolean.parseBoolean(params[1]);
                    if (dp != null && dp.isOutput()) {
                        dp.setValue(value);
                    }

                    sendResponse(webSocket, label, String.valueOf(value));
                }
            }

            {
                if(DataPoint.getDataPointFromLabel(label)instanceof FloatDataPoint) {
                    FloatDataPoint dp = (FloatDataPoint) DataPoint.getDataPointFromLabel(label);

                    float value = Float.parseFloat(params[1]);
                    if (dp != null && dp.isOutput()) {
                        dp.setValue(value);
                    }
                    sendResponse(webSocket, label, String.valueOf(value));
                }
            }
        } else {
            DEBUG_INFO("onMessage()", "Error: invalid message !");
        }

    }

    /** Called when errors occurs
     *
     * @param webSocket instance for one active TCP connection in the server
     * @param e Exception error
     */
    @Override
    public void onError(WebSocket webSocket, Exception e) {
        DEBUG_INFO("onError()", "Exception: " + e.getLocalizedMessage());

        e.printStackTrace();
        // Just close the _connection.
        //connection.close(2);
    }

    /** Informs when the web server is started
     *
     */
    @Override
    public void onStart() {
        System.out.println("Server web start");
    }

    /** Set the value of the label in the web page
     *
     * @param connection instance for one active TCP connection in the server
     * @param label The label of the Datapoint
     * @param value THe value of the Datapoint
     * @return true if the sendResponse is send, if isn't return false
     */
        private boolean sendResponse(WebSocket connection, String label, String value) {
            if (connection == null) {
                return false;
            }
            if (label == null || label.length() == 0) {
                return false;
            }
            if (value == null || value.length() == 0) {
                return false;
            }

            DEBUG_INFO("sendResponse()",label + "=" + value);


            return true;
        }

    /**
     *
     * @param method
     * @param msg
     */
        // DEBUG System.out
        private void DEBUG_INFO(String method, String msg) {
            if(DEBUG_ENABLE)
                Utility.DEBUG("[webSocket::WebConnector]", method, msg);
        }


}
