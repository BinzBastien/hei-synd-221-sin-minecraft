package ch.hevs.isi.core;

import ch.hevs.isi.core.DataPoint;

public class FloatDataPoint extends DataPoint {
    private float _value;

    /** Class constructor.
     *
     * @param label The DataPoint Label.
     * @param isOutput True for output, false for input.
     */
    public FloatDataPoint(String label, boolean isOutput) {
        super(label, isOutput);
    }

    /** Set a float value to the FloatDataPoint.
     *
     * @param value The float value to set.
     */
    public void setValue(float value){
        this._value = value;

        if (this.isOutput()){
            dbc.onNewValueFloat(this);
            wc.onNewValueFloat(this);
            fc.onNewValueFloat(this);
        } else {
            dbc.onNewValueFloat(this);
            wc.onNewValueFloat(this);
        }
    }

    /** Get the float value of the FloatDataPoint.
     *
     * @return The float value.
     */
    public float getValue(){
        return _value;
    }
}
