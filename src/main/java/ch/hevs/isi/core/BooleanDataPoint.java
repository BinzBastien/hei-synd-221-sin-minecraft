package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;

public class BooleanDataPoint extends DataPoint {
    private boolean _value;

    /** Class constructor.
     *
     * @param label The DataPoint Label.
     * @param isOutput True for output, false for input.
     */
    public BooleanDataPoint(String label, boolean isOutput) {
        super(label, isOutput);
    }

    /** Set a boolean value to the BooleanDataPoint.
     *
     * @param value The boolean value to set.
     */
    public void setValue(boolean value){
        this._value = value;

        if (this.isOutput()){
            dbc.onNewValueBoolean(this);
            fc.onNewValueBoolean(this);
        } else {
            dbc.onNewValueBoolean(this);
            wc.onNewValueBoolean(this);
        }

    }

    /** Get the boolean value of the BooleanDataPoint.
     *
     * @return The boolean value.
     */
    public boolean getValue(){
        return _value;
    }

}
