package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

import java.util.HashMap;

public class DataPoint {

    private String _label;
    private Boolean _isOutput;

    protected static DatabaseConnector dbc = DatabaseConnector.getInstance();
    protected static WebConnector wc = WebConnector.getInstance();
    protected static FieldConnector fc = FieldConnector.getInstance();

    private static HashMap<String, DataPoint> _dpMap = new HashMap<>();

    /** Get the label of the DataPoint.
     *
     * @return The label of the DataPoint.
     */
    public String getLabel() {
        return _label;
    }

    /** Class constructor, it put automatically the new DataPoint in the HashMap _dpMap.
     *
     * @param label The label of the DataPoint.
     * @param isOutput True for output, false for input.
     */
    public DataPoint(String label, boolean isOutput){
        this._label = label;
        this._isOutput = isOutput;
        _dpMap.put(label, this);
        //System.out.println(_dpMap.toString());
    }

    /** Get if the DataPoint is an output or an input
     *
     * @return True for output, false for input.
     */
    public boolean isOutput(){
        return _isOutput;
    }

    /** Get a DataPoint from the HashMap _dpMap.
     *
     * @param label The label of the dataPoint.
     * @return The selected DataPoint.
     */
    public static DataPoint getDataPointFromLabel(String label){
        return _dpMap.get(label);
    }


}