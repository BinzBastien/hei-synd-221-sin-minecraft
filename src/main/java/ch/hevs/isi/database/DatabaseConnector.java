package ch.hevs.isi.database;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

public class DatabaseConnector {

    private static DatabaseConnector dbc = null;

    /** If no instance, create an unique instance of the class. If an instance exist, it return it to avoid multiple instances.
     *
     * @return The instance of the DatabaseConnector.
     */
    public static DatabaseConnector getInstance() {
        if (dbc == null) {
            dbc = new DatabaseConnector();
        }
        return dbc;
    }

    /** Push to the Database a label with a float value.
     *
     * @param label The label to push.
     * @param value The float value corresponding to the label.
     */
    private void pushToDatabase(String label, float value) {

        try {
            URL url = new URL("https://influx.sdi.hevs.ch/write?db=SIn16");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            String userpass = "SIn16:4dae48e9267d952ba12a0bb19303ca62";
            String encoding =  Base64.getEncoder().encodeToString(userpass.getBytes());
            connection.setRequestProperty ("Authorization", "Basic " + encoding);

            connection.setRequestProperty("Content-Type", "binary/octet-stream");

            connection.setRequestMethod("POST");

            connection.setDoOutput(true);

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());




            writer.write(label + " value=" + value  );
            writer.flush();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));


            int responseCode = connection.getResponseCode();

            while ((in.readLine()) != null) {}


        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    /** Push to the Database a label with a boolean value.
     *
     * @param label The label to push.
     * @param value The boolean value corresponding to the label.
     */
    private void pushToDatabase(String label, boolean value) {
        try {
            URL url = new URL("https://influx.sdi.hevs.ch/write?db=SIn16");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            String userpass = "SIn16:4dae48e9267d952ba12a0bb19303ca62";
            String encoding =  Base64.getEncoder().encodeToString(userpass.getBytes());
            connection.setRequestProperty ("Authorization", "Basic " + encoding);

            connection.setRequestProperty("Content-Type", "binary/octet-stream");

            connection.setRequestMethod("POST");

            connection.setDoOutput(true);

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());




            writer.write(label + " value=" + value  );
            writer.flush();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));


            int responseCode = connection.getResponseCode();

            while ((in.readLine()) != null) {}


        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** Push to the Database with the new value of a FloatDataPoint.
     *
     * @param fdp The FloatDataPoint to push.
     */
    public void onNewValueFloat(FloatDataPoint fdp) {
        pushToDatabase(fdp.getLabel(), fdp.getValue());
    }

    /** Push to the Database with the new value of a BooleanDataPoint.
     *
     * @param bdp The BooleanDataPoint to push.
     */
    public void onNewValueBoolean(BooleanDataPoint bdp) {
        pushToDatabase(bdp.getLabel(), bdp.getValue());
    }


}