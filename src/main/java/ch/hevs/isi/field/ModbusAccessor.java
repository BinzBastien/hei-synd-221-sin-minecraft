package ch.hevs.isi.field;

import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.ip.tcp.TcpMaster;
import com.serotonin.modbus4j.locator.BaseLocator;

public class ModbusAccessor extends ModbusFactory {


    IpParameters params;
    TcpMaster master;

    private static ModbusAccessor ma = null;

    /** If no instance, create and init an unique instance of the class. If an instance exist, it return it to avoid multiple instances.
     *
     * @return The instance of the ModbusAccessor.
     */
    public static ModbusAccessor getInstance(){
        if (ma == null) {
            ma = new ModbusAccessor();

            try {
                ma.master.init();
            } catch (ModbusInitException e) {
                e.printStackTrace();
            }
        }
        return ma;
    }

    /** Class constructor, with port 1502 and host "localHost".
     *
     */
    private ModbusAccessor() {
        params = new IpParameters();
        params.setHost("localHost");
        params.setPort(1502);

        master = (TcpMaster) createTcpMaster(params, true);
    }

    /** Get the float value from a specified register.
     *
     * @param regAddress The register address.
     * @return The float value of the register.
     */
    public float readFloat(int regAddress) {
        float value =0f;
        try {


             value = (float) master.getValue(BaseLocator.inputRegister(1, regAddress, DataType.FOUR_BYTE_FLOAT));
        }
        catch (ModbusTransportException | ErrorResponseException e) {
            e.printStackTrace();
        }


        return value;
    }

    /** Write a float value to a specified register.
     *
     * @param regAddress The register address.
     * @param newValue The float value to write in the register.
     */
    public void writeFloat(int regAddress, float newValue) {
        try {

            master.setValue(BaseLocator.holdingRegister(1, regAddress, DataType.FOUR_BYTE_FLOAT), newValue);

        }
        catch (ModbusTransportException | ErrorResponseException e) {
            e.printStackTrace();
        }
    }

    /** Get the boolean value from a specified register.
     *
     * @param regAddress The register address.
     * @return The boolean value of the register.
     */
    public boolean readBoolean(int regAddress) {
        boolean value =false;
        try {


            if((float) master.getValue(BaseLocator.inputRegister(1, regAddress, DataType.FOUR_BYTE_FLOAT))==1f) {
                value = true;
            }
            else {
                value = false;
            }
        }
        catch (ModbusTransportException | ErrorResponseException e) {
            e.printStackTrace();
        }


        return value;
    }

    /** Write a boolean value to a specified register.
     *
     * @param regAddress The register address.
     * @param newValue The boolean value to write in the register.
     */
    public void writeBoolean(int regAddress, boolean newValue) {
        float value;
        if (newValue) {
            value = 1.f;
        } else {
            value = 0.f;
        }

        try {

            master.setValue(BaseLocator.holdingRegister(1, regAddress, DataType.FOUR_BYTE_FLOAT), value);

        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        }

    }

    // Test the class

    /*public static void main(String[] args) {
        ModbusAccessor ma = new ModbusAccessor();

        try {

            ma.master.init();
            System.out.println("Client initialized");

            ma.writeFloat(205, 0.5f);   // put the factory setPoint to 75%
            ma.writeBoolean(405, true); // set the turbine off
            ma.readFloat(89);                   // Current voltage of the DC Grid
            ma.readBoolean(613);                   // State of turbine

            System.out.println("Task finished");

        } catch (ModbusInitException e) {
            System.out.println("Error : " + e);
        }

    }*/

}
