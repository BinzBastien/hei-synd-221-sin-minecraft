package ch.hevs.isi.field;


import ch.hevs.isi.core.BooleanDataPoint;

public class BooleanRegister {

    private int address;
    public BooleanDataPoint bdp;

    /** Class constructor, it create a BooleanDataPoint and set the address.
     *
     * @param address The address of the BooleanRegister.
     * @param label The label of the BooleanDataPoint.
     */
    public BooleanRegister(int address, String label,boolean isOutput) {

        bdp = new BooleanDataPoint(label, isOutput);
        this.address = address;

    }

    /** Set the read value from the server to the BooleanDataPoint.
     *
     */
    public void read() {
        bdp.setValue(ModbusAccessor.getInstance().readBoolean(address));
    }

    /** Write the value of the BooleanDataPoint to the server.
     *
     */
    public void write() {
        boolean value = bdp.getValue();
        ModbusAccessor.getInstance().writeBoolean(address, value);
    }

    // Test the class

    /*public static void main(String[] args) {

        BooleanRegister br = new BooleanRegister(613, "WIND_CONNECT_SW");
        br.read();
        System.out.println(br.bdp.getValue());
    }*/


}
