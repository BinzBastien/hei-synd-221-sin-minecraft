package ch.hevs.isi.field;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

public class FloatRegister{
    private int address;
    private int range;
    private int offset;
    private FloatDataPoint fdp;

    /** Class constructor, it create a FloatDataPoint and set the address, the range and the offset.
     *
     * @param address The FloatRegister address.
     * @param label The FloatRegister label.
     * @param range The range of the values.
     * @param offset The offset of the values.
     */
    public FloatRegister(int address, String label, int range, int offset, boolean isOutput) {
        fdp = new FloatDataPoint(label, isOutput);
        this.address = address;
        this.range = range;
        this.offset = offset;
    }

    /** Set the read value from the server to the FloatDataPoint.
     *
     */
    public void read (){
        fdp.setValue(ModbusAccessor.getInstance().readFloat(address) * range + offset);



    }

    /** Write the value of the FloatDataPoint to the server.
     *
     */
    public void write(){
        float value = (fdp.getValue() - offset) / range;
        ModbusAccessor.getInstance().writeFloat(address,value);
    }

}
