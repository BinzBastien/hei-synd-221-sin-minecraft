package ch.hevs.isi.field;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class FieldConnector {

    private static FieldConnector fc = null;
    private  HashMap<DataPoint,FloatRegister> _fdpMap = new HashMap<>();
    private  HashMap<DataPoint,BooleanRegister> _bdpMap = new HashMap<>();

    /** If no instance, create an unique instance of the class. If an instance exist, it return it to avoid multiple instances.
     *
     * @return The instance of the FieldConnector.
     */
    public static FieldConnector getInstance(){
        if (fc == null) {
            fc = new FieldConnector();
        }
        return fc;
    }


    /** Create a FloatRegister with the specified parameters.
     *
     * @param address The address of the FloatRegister.
     * @param label The label of the FloatRegister.
     * @param offset The offset of the values.
     * @param range The range of the values.
     */
    public void createFloatRegister (int address, String label, int range, int offset,boolean isOutput){
        FloatRegister fr = new FloatRegister(address,label, range, offset,isOutput);

        _fdpMap.put(FloatDataPoint.getDataPointFromLabel(label),fr);


    }

    /** Create a BooleanRegister with the specified parameters.
     *
     * @param address The address of the BooleanRegister.
     * @param label The label of the BooleanRegister.
     */
    public void createBooleanRegister (int address, String label,boolean isOutput){
        BooleanRegister br = new BooleanRegister(address,label,isOutput);

        _bdpMap.put(BooleanDataPoint.getDataPointFromLabel(label),br);



    }

    /** Get the register of a specified FloatDataPoint.
     *
     * @param fdp The FloatDataPoint.
     * @return The FloatRegister from fdp.
     */
    public  FloatRegister getRegisterFromDataPoint(FloatDataPoint fdp){
        return _fdpMap.get(fdp);

    }

    /** Get the register of a specified BooleanDataPoint.
     *
     * @param bdp The BooleanDataPoint.
     * @return The BooleanRegister from bdp.
     */
    public BooleanRegister  getRegisterFromDataPoint(BooleanDataPoint bdp){
        return _bdpMap.get(bdp);
    }

    /** Write to the register a new value of a FloatDataPoint.
     *
     * @param fdp The FloatDataPoint with a new value.
     */
    public void onNewValueFloat(FloatDataPoint fdp) {
        getRegisterFromDataPoint(fdp).write();
    }

    /** Write to the register a new value of a BooleanDataPoint.
     *
     * @param bdp The BooleanDataPoint with a new value.
     */
    public void onNewValueBoolean(BooleanDataPoint bdp) {
        getRegisterFromDataPoint(bdp).write();
    }


    /** Start the polling task, for get every 10sec all the data.
     *
     */
    public void startPolling(){
        Timer pollTimer = new Timer();
        pollTimer.scheduleAtFixedRate(new PollTask(), 0, 10000);
    }

    private class PollTask extends TimerTask {
        @Override
        public void run() {


            for (FloatRegister fr : _fdpMap.values())
            {
                fr.read();

            }

            for (BooleanRegister br : _bdpMap.values())
            {
                br.read();

            }
        }
    }

    // for class test

    /*public static void main(String[] args) {

        FieldConnector fc = new FieldConnector();

        fc.startPolling();

    }*/

}

