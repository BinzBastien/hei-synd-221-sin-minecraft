package ch.hevs.isi;

import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.utils.Utility;
import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.web.SmartControl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MinecraftController {
    public static boolean USE_MODBUS4J = false;

    public static void usage() {
        System.out.println("Parameters: <InfluxDB Server> <Group Name> <MordbusTCP Server> <modbus TCP port> [-modbus4j]");
        System.exit(1);
    }

    public static void main(String[] args) {

        String dbHostName    = "localhost";
        String dbName        = "labo";
        String dbUserName    = "root";
        String dbPassword    = "root";

        String modbusTcpHost = "localhost";
        int    modbusTcpPort = 1502;

        // Check the number of arguments and show usage message if the number does not match.
        String[] parameters = null;

        // If there is only one number given as parameter, construct the parameters according the group number.
        if (args.length == 4 || args.length == 5) {
            parameters = args;

            // Decode parameters for influxDB
            dbHostName  = parameters[0];
            dbUserName  = parameters[1];
            dbName      = dbUserName;
            dbPassword  = Utility.md5sum(dbUserName);

            // Decode parameters for Modbus TCP
            modbusTcpHost = parameters[2];
            modbusTcpPort = Integer.parseInt(parameters[3]);

            if (args.length == 5) {
                USE_MODBUS4J = (parameters[4].compareToIgnoreCase("-modbus4j") == 0);
            }
        } else {
            usage();
        }
        FieldConnector fc = FieldConnector.getInstance();


        try {

            BufferedReader csvReader = Utility.fileParser("csvfile","Modbus.csv");
            String row;
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split(";");
                if(data.length>4) {
                    if (data[1].equals("Y")) {
                        fc.createFloatRegister(Integer.parseInt(data[2]), data[0], Integer.parseInt(data[3]), Integer.parseInt(data[4]),true);
                    }
                    else{
                        fc.createFloatRegister(Integer.parseInt(data[2]), data[0], Integer.parseInt(data[3]), Integer.parseInt(data[4]),false);

                    }
                }
                else{
                    if (data[1].equals("Y")) {
                        fc.createBooleanRegister(Integer.parseInt(data[2]),data[0],true);

                    }
                    else{
                        fc.createBooleanRegister(Integer.parseInt(data[2]),data[0],false);

                    }

                }
            }
            csvReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        SmartControl sc = SmartControl.getInstance();
        fc.startPolling();




    }
}
